package com.ps.pocdayview

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.Gravity
import java.text.SimpleDateFormat
import java.util.*

import android.widget.RelativeLayout
import android.widget.LinearLayout
import android.widget.Toast
import com.ps.pocdayview.databinding.DayViewTimeBinding
import com.tooltip.Tooltip

import kotlinx.android.synthetic.main.event_item.view.*


class MainActivity : AppCompatActivity() {
    var step = 0
    lateinit var binding: DayViewTimeBinding

    private var dailyEvent = mutableListOf<DayEvent>()

    private var eventIndex: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.day_view_time)

        eventIndex = binding.leftEventColumn.childCount

        displayDailyEvents()
    }


    private fun displayDailyEvents() {
        val start = Calendar.getInstance()
        start.set(Calendar.HOUR_OF_DAY, 17)
        start.set(Calendar.MINUTE, 0)
        val end = Calendar.getInstance()
        end.set(Calendar.HOUR_OF_DAY, 19)
        end.set(Calendar.MINUTE, 0)
        var h = getEventTimeFrame(start.time, end.time)
        var event = DayEvent(start.time, end.time, "TEST EVENT 0", h)
        //dailyEvent.add(event)

        start.set(Calendar.HOUR_OF_DAY, 9)
        start.set(Calendar.MINUTE, 0)
        end.set(Calendar.HOUR_OF_DAY, 9)
        end.set(Calendar.MINUTE, 30)
        h = getEventTimeFrame(start.time, end.time)
        event = DayEvent(start.time, end.time, "TEST EVENT 1", h)
        dailyEvent.add(event)
//
        start.set(Calendar.HOUR_OF_DAY, 9)
        start.set(Calendar.MINUTE, 0)
        end.set(Calendar.HOUR_OF_DAY, 10)
        end.set(Calendar.MINUTE, 0)
        h = getEventTimeFrame(start.time, end.time)
        event = DayEvent(start.time, end.time, "TEST EVENT 2  ", h)
        dailyEvent.add(event)
////
        start.set(Calendar.HOUR_OF_DAY, 9)
        start.set(Calendar.MINUTE, 0)
        end.set(Calendar.HOUR_OF_DAY, 10)
        end.set(Calendar.MINUTE, 0)
        h = getEventTimeFrame(start.time, end.time)
        event = DayEvent(start.time, end.time, "TEST EVENT 3", h)
        dailyEvent.add(event)
//
        start.set(Calendar.HOUR_OF_DAY, 6)
        start.set(Calendar.MINUTE, 0)
        end.set(Calendar.HOUR_OF_DAY, 8)
        end.set(Calendar.MINUTE, 0)
        h = getEventTimeFrame(start.time, end.time)
        event = DayEvent(start.time, end.time, "TEST EVENT 4", h)
        dailyEvent.add(event)
////
        start.set(Calendar.HOUR_OF_DAY, 6)
        start.set(Calendar.MINUTE, 30)
        end.set(Calendar.HOUR_OF_DAY, 9)
        end.set(Calendar.MINUTE, 30)
        h = getEventTimeFrame(start.time, end.time)
        event = DayEvent(start.time, end.time, "TEST EVENT 6 ", h)
        dailyEvent.add(event)

        dailyEvent.sortByDescending { it.minutes }

        for (i in 0..dailyEvent.lastIndex) {
            //for (eObject in dailyEvent) {
            val eventDate = dailyEvent[i].start
            val endDate = dailyEvent[i].end
            val eventMessage = dailyEvent[i].message
            val eventBlockHeight = getEventTimeFrame(eventDate, endDate)
            Log.d("PocDayView", "Height $eventBlockHeight")
            step = 0
            displayEventSection(eventDate, eventBlockHeight, eventMessage, i)
            //}
        }
    }

    private fun getEventTimeFrame(start: Date, end: Date): Int {
        val timeDifference = end.time - start.time
        val mCal = Calendar.getInstance()
        mCal.timeInMillis = timeDifference
        return (mCal.timeInMillis / 60000).toInt()
    }

    private fun displayEventSection(
        eventDate: Date,
        height: Int,
        message: String,
        index: Int
    ) {
        val timeFormatter = SimpleDateFormat("HH:mm", Locale.ENGLISH)
        val displayValue = timeFormatter.format(eventDate)
        val hourMinutes = displayValue.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val hours = Integer.parseInt(hourMinutes[0])
        val minutes = Integer.parseInt(hourMinutes[1])
        Log.d("PocDayView", "Hour value $hours")
        Log.d("PocDayView", "Minutes value $minutes")
        val topViewMargin = (hours * 60 + minutes) + 10
        Log.d("PocDayView", "Margin top $topViewMargin")
        createEventCard(topViewMargin, height, message, index)
    }


    private fun createEventCard(
        topMargin: Int,
        height: Int,
        message: String,
        index: Int
    ) {
        val eventLayout = layoutInflater.inflate(R.layout.event_item, null)
        val lParam =
            RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        lParam.addRule(RelativeLayout.ALIGN_PARENT_TOP)
        lParam.topMargin = topMargin * 2
        val step = calStepRight(index)
        var leftMargin = 0
        if (step == 0) {
            leftMargin = 0
        } else {
            leftMargin = (280 * step) + (30 * step)
        }

        lParam.leftMargin = leftMargin

        eventLayout.layoutParams = lParam

        eventLayout.tvEventName.height = height * 2
        eventLayout.tvEventName.width = 250
        eventLayout.tvEventName.text = message
        eventLayout.tvEventName.maxLines = 1
        eventLayout.tvEventName.ellipsize = TextUtils.TruncateAt.END
        eventLayout.tvEventName.gravity = Gravity.CENTER

        eventLayout.tvNumber.text = ""

        eventLayout.setOnClickListener {
            Tooltip.Builder(eventLayout)
                .setCancelable(true)
                .setOnClickListener {   Toast.makeText(this, message, Toast.LENGTH_SHORT).show()}
                .setText("เปิดพิธีแข่งบาสจังหวัด\nLocation : สนามบาส\nSchedule : 10:00 - 12.00").show()

        }
        binding.leftEventColumn.addView(eventLayout, eventIndex - 1)

    }

    private fun calStepRight(currentIndex: Int): Int {
        return calStep(dailyEvent.subList(0, currentIndex), dailyEvent[currentIndex])
    }

    private fun filterList(dataList: List<DayEvent>, data: DayEvent): MutableList<DayEvent> {
        val fList = mutableListOf<DayEvent>()
        val current = data
        for (item in dataList) {
            val a = item.start.time < current.start.time
            val b = item.end.time < current.end.time
            val c = item.end.time <= current.start.time
            val d = item.start.time > current.start.time
            val e = item.start.time >= current.end.time
            val f = item.end.time > current.end.time

            if (a && b && c) {
                //step += 0
            } else if (d && e && f) {
                //step += 0
            } else {
                fList.add(item)
            }
        }
        return fList
    }

    fun calStep(dataList: List<DayEvent>, data: DayEvent): Int {
        val tt = filterList(dataList, data)
        if (tt.size > 0) {
            step += 1
            calStep(tt.subList(0, tt.lastIndex), tt[tt.lastIndex])
        }
        return step
    }
}
